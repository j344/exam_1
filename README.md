## Code Submission Requirements:
 1. Use of any scripting language is fine, but preferably python or bash.
 2. Your script should be accompanied by a Readme.md file that contains a brief description and usage of your script.
 3. If your script will require a special yum/deb package, indicate the exact package name and version number in the Readme.md file.
 4. Submission of code is thru bitbucket or github or any public git repo you prefer. Send the git repo url to jefferson@techasiaportal.com 

## Instruction


- Create a docker-compose file that will run nginx:1.19.x on port 8080.
    * the logs access.log and error.log should be redirected to folder named nginx-logs on the host machine 
    
          ![structure][structure]
          
    * Make the files in the current working directory as a path in nginx as `localhost:8080/cwd`
    
          ![index][index]
          
```
$ curl localhost:8080/cwd/index.html
Sample
        
$ curl -s localhost:8080/cwd/Readme.md | head -1
Code Submission Requirements:
```

- Write a script that will monitor the nginx access.log file and show continously how many a particular HTTP code has appeared since the script was executed.

## Optional

Use inotify to monitor access.log

## Example Output
```
$ yourscript.sh nginx-logs/access.log

# the output should be something like this.
======= Starting
http code 200: 1
http code 404: 2
http code 444: 2
========
http code 200: 2
http code 404: 2
http code 444: 2
========
http code 200: 3
http code 404: 2
http code 444: 2
========
http code 200: 3
http code 404: 2
http code 444: 3
========
http code 200: 3
http code 404: 2
http code 444: 4
http code 405: 2
```


[structure]: structure.png 
[index]: index.png

